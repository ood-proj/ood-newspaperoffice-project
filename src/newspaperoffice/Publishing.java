package newspaperoffice;

public class Publishing extends JournalState {
    private JournalModel jm;



    public Publishing(JournalModel jm) {
        this.jm = jm;
    }

    public Publishing() {
    }

    public void setJm(JournalModel jm) {
        this.jm = jm;
    }



    public void notifySubscribers(String status){
        for(Subscriber s : jm.getSubscribers()){

            s.updateStatus(status);
        }
    }

    public JournalModel getJm() {
        return jm;
    }
}
