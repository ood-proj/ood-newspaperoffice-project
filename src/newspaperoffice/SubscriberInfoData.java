package newspaperoffice;

/*  -----------------------------------------------------------------------------------
    class SubscriberInfoData
    to convert subscriber's personal information to server's demanding format
 -----------------------------------------------------------------------------------*/
public class SubscriberInfoData {
    public static String getEncodedData(PersonalInformation personalInformation) {
        return new StringBuilder(personalInformation.toString()).reverse().toString();
    }
}
