package newspaperoffice;

public class AddQDecorator extends OutputStringDecorator {

    public AddQDecorator(OutputString component){
        this.component = component;
    }

    public AddQDecorator(){

    }




    @Override
    public String modelToString() {
        return "? "+component.modelToString();
    }
}
