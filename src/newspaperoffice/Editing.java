package newspaperoffice;

import java.util.Random;

public class Editing extends JournalState {
    private int percent = 0;
    public int progress()
    {
        Random rand = new Random();
        percent = rand.nextInt( 101-percent)+percent;
        return percent;
    }
}
