package newspaperoffice;

public abstract class DeliverStrategy {
    public abstract void deliver(Publishing publish);

    public static DeliverStrategy getDeliveryStrategyByName(String name){
        switch (name){
            case Constants.DeliveryMethodStrings.BIKE_DELIVERY:
                return new BikeDelivery();
            case Constants.DeliveryMethodStrings.ONLINE_DOWNLOAD:
                return new OnlineDownload();
            default:
                throw new DeliveryMethodNotFound();

        }

    }

    @Override
    public String toString() {
        return this.getClass().getName();
    }
}

class DeliveryMethodNotFound extends RuntimeException{


}
