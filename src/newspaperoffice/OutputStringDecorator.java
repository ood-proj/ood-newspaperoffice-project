package newspaperoffice;

public abstract class OutputStringDecorator extends OutputString {
    protected OutputString component;


    @Override
    public String modelToString() {

        return component.modelToString();
    }

    public void setComponent(OutputString component) {
        this.component = component;
    }

}
