package newspaperoffice;


import java.util.*;
/*  -----------------------------------------------------------------------------------
    class NewspaperOffice
    main newspaper office
    keeping records of all journalmodels and all subscribers
    responsible for decorating a journalmodels info string
    initFromStorage boolean determines whether to load and use previously saved data from storage or not
 -----------------------------------------------------------------------------------*/
public class NewspaperOffice {
    private ArrayList<Subscriber> allSubscribers = new ArrayList<Subscriber>();
    private ArrayList<JournalModel> allJournalModels = new ArrayList<>();
    private final LinkedHashSet<String> journalModelInfoDecorators = new LinkedHashSet<>();
    private SubscriberSaver subscriberSaver = (SubscriberSaver) SubscriberSaver.getUniqueInstance("subscribers.csv");
    private JournalModelSaver journalModelSaver = (JournalModelSaver) JournalModelSaver.getUniqueInstance("journal_models.csv");

    public NewspaperOffice(boolean initFromStorage) {
        if (initFromStorage) {

            try {
                allSubscribers = subscriberSaver.retrieve();
                allJournalModels = journalModelSaver.retrieve();
                for (JournalModel jm : allJournalModels) {
                    int id;
                    for (SubscriberInfo info : jm.getSubscriberInfos()) {
                        id = Integer.parseInt(info.getData().split(" ")[0]);
                        jm.addNewSubscriberReference(allSubscribers.get(id));
                    }
                }
                System.out.println(allSubscribers.size() + " record of subscribers loaded");
                System.out.println(allJournalModels.size() + " record of journal models loaded");
            } catch (Exception e) {
                System.out.println("LOG : Could not retrieve journal models");
                e.printStackTrace();
            }
        }
    }
    /*  -----------------------------------------------------------------------------------
        method addNewJournalModel
        Requests for new JournalModel instance
        Adds new instance to it's own list of JournalModel instances
        -----------------------------------------------------------------------------------*/
    public void addNewJournalModel(String name){
        JournalModel.giveKeyTo(this);
        this.allJournalModels.add(this.key.getJournalInstanceByName(name));
    }

    public void printAllData() {
        for (JournalModel jm : allJournalModels) {
            try {
                String decoratedString = getDecoratedString(jm);
                if (decoratedString == null) throw new AssertionError();
                System.out.println(decoratedString);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    /*  -----------------------------------------------------------------------------------
    method getDecoratedString
    receives journal model as input and returns decorated string according to previously registered decorators in decorators object
    -----------------------------------------------------------------------------------*/
    private String getDecoratedString(JournalModel jm) {
        OutputStringDecorator result = null;
        Iterator<String> iter = journalModelInfoDecorators.iterator();
        if (!iter.hasNext()) return jm.modelToString();

        try {
            result = (OutputStringDecorator) Class.forName(iter.next()).newInstance();
            result.setComponent(jm);
            OutputStringDecorator prevDecorator = result;
            while (iter.hasNext()) {

                result = (OutputStringDecorator) Class.forName(iter.next()).newInstance();
                result.setComponent(prevDecorator);

            }
        } catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
            e.printStackTrace();
        }

        return result.modelToString();
    }



    public void addDecorator(String decoratorName) {
        journalModelInfoDecorators.add(decoratorName);
    }

    public void removeDecorator(String decoratorName) {
        journalModelInfoDecorators.remove(decoratorName);
    }


    public JournalModel getJournalByName(String name) throws NullPointerException {
        for (JournalModel jm : allJournalModels) {
            if (jm.getName().equals(name))
                return jm;
        }

        throw new NullPointerException();
    }



    public void saveToStorage() {
        journalModelSaver.saveList(allJournalModels);
        subscriberSaver.saveList(allSubscribers);
    }
    private JournalModel.JournalModelFriends key;

    public void receiveKey(JournalModel.JournalModelFriends key) {
        this.key = key;
    }


    public ArrayList<Subscriber> getAllSubscribers() {
        return allSubscribers;
    }
}
class WrongSubscriberId extends RuntimeException{

}