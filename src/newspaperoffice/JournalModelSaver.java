package newspaperoffice;

import java.io.IOException;
import java.util.ArrayList;

public class JournalModelSaver extends ObjectSaver {

    private static ObjectSaver uniqueInstance;

    private JournalModelSaver(String fileName) {
        this.fileName = fileName;
    }

    static ObjectSaver getUniqueInstance(String fileName) {
        if (uniqueInstance == null) {
            uniqueInstance = new JournalModelSaver(fileName);
        }
        return uniqueInstance;
    }

    void saveList(ArrayList<JournalModel> journalModels) {
        ArrayList<String[]> dataLines = new ArrayList<>();
        for (JournalModel jm : journalModels) {
            ArrayList<String> subsInfoString = new ArrayList<>();
            subsInfoString.add(jm.getName());
            subsInfoString.add(jm.getDate());
            subsInfoString.add(jm.getHead());
            subsInfoString.add(jm.getState().toString());
            subsInfoString.add(jm.getDeliveryStrategy().toString());
            for (SubscriberInfo subsInfo : jm.getSubscriberInfos()) {
                subsInfoString.add(subsInfo.getData());
            }
            dataLines.add(subsInfoString.toArray(new String[0]));
        }

        try {
            saveDataArrayToFile(dataLines);
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    ArrayList<JournalModel> retrieve( ) throws ClassNotFoundException, IllegalAccessException, InstantiationException {
        ArrayList<JournalModel> result = new ArrayList<>();
        ArrayList<String[]> csvContent = this.readFromCSV();
        JournalModel.giveKeyTo(this);
        for (String[] objContent : csvContent) {

            JournalState state = (JournalState) Class.forName(objContent[3]).newInstance();
            DeliverStrategy deliverStrategy = (DeliverStrategy) Class.forName(objContent[4]).newInstance();

            JournalModel retrievedJm = this.journalModelKey.getJournalInstance(objContent[0] , objContent[1] , objContent[2] , state , deliverStrategy );
            if (state instanceof Publishing) ((Publishing) state).setJm(retrievedJm);
            for (int i = 5; i < objContent.length; ++i) {
                retrievedJm.addNewSubscriberInfo(objContent[i]);
            }
            result.add(retrievedJm);
        }
        return result;
    }

    private JournalModel.JournalModelFriends journalModelKey;

    public void receiveKey(JournalModel.JournalModelFriends key) {
        this.journalModelKey = key;
    }



}
