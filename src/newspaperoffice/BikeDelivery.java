package newspaperoffice;

public class BikeDelivery extends DeliverStrategy {
    public void deliver(Publishing publish) {
        for(SubscriberInfo i : publish.getJm().getSubscriberInfos())
            System.out.println("BikeDelivery: character "+i.getData().split(" ")[0] + " got notified!");
        System.out.println("End of notifying");
    }
}
