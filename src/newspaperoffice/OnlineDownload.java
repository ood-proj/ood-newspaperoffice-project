package newspaperoffice;

public class OnlineDownload extends DeliverStrategy {
    public void deliver(Publishing publish)
    {
        for(SubscriberInfo i : publish.getJm().getSubscriberInfos())
            System.out.println("OnlineDelivery: sent file to Character "+i.getData().split(" ")[0]);
        System.out.println("End of notifying");
    }

}
