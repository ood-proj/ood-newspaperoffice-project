package newspaperoffice;

import java.util.List;

public class Subscriber {
    private int id;
    private PersonalInformation personalInformation;
    private String status;


    public PersonalInformation getPersonalInformation() {
        return personalInformation;
    }

    public Subscriber(int id, PersonalInformation personalInformation) {
        this.id = id;
        this.personalInformation = personalInformation;
    }

    public String getStatus() {
        return status;
    }

    public void updateStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "Subscriber{" +
                "id=" + id +
                ", personalInformation=" + personalInformation +
                '}';
    }

    public int getId() {
        return id;
    }
}
