package newspaperoffice;

public class AddStarDecorator extends OutputStringDecorator {

    public AddStarDecorator() {
    }

    public AddStarDecorator(OutputString component) {
        this.component = component;
    }

    @Override
    public String modelToString() {
        return "* " + component.modelToString();
    }
}
