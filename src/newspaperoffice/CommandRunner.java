package newspaperoffice;

import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class CommandRunner {

    public static void main(String[] args) {

        String inputLine, name;
        int id, i = 0;
        Scanner scan = new Scanner(System.in);
        NewspaperOffice newspaperOffice = new NewspaperOffice(true);
        Runtime.getRuntime().addShutdownHook(new Thread() {
            @Override
            public void run() {
                newspaperOffice.saveToStorage();
            }
        });
        while (true) {
            System.out.println("\n>");
            inputLine = scan.nextLine();

            switch (inputLine.split(" ")[0]) // Command identifier = inputLine.split(" ")[0]
            {
                case "printInfo":
                    //
                    newspaperOffice.printAllData();
                    break;
                case "addQ":
                    //
                    newspaperOffice.addDecorator(AddQDecorator.class.getName());
                    break;
                case "removeQ":
                    //
                    newspaperOffice.removeDecorator(AddQDecorator.class.getName());
                    break;
                case "addStar":
                    //
                    newspaperOffice.addDecorator(AddStarDecorator.class.getName());
                    break;
                case "removeStar":
                    //
                    newspaperOffice.removeDecorator(AddStarDecorator.class.getName());
                    break;
                case "createModel":
                    //

                    try {
                        name = getStringsBetweenDoubleQuotes(inputLine).get(0);
                        newspaperOffice.addNewJournalModel(name);
                    } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
                        System.out.println("Wrong input format , consider entering names with double qoutations like \"sport\"");
                    } catch (JournalModelAlreadyExists journalModelExists) {
                        System.out.println("Sorry journal already exists");
                    } catch (JournalModelNameNotAllowed nameNotAllowed){
                        System.out.println("The name you entered is not allowed");
                    }
                    break;
                case "subscribe":
                    try {
                        name = getStringsBetweenDoubleQuotes(inputLine).get(0); // JournalName
                        String[] parts = inputLine.split(" ");
                        id = Integer.parseInt(parts[parts.length - 1]);
                        if(!newspaperOffice.getJournalByName(name).subscribed(id)){
                            if(id >= newspaperOffice.getAllSubscribers().size() || id < 0) throw new WrongSubscriberId();
                            SubscriberInfo result = newspaperOffice.getJournalByName(name).registerSubscriber(newspaperOffice.getAllSubscribers().get(id));
                            System.out.println(result.getData());
                        }else{
                            System.out.println("Already subscribed!");
                        }

                    } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
                        System.out.println("Wrong input format , consider entering names with double quotations like \"sport\"");
                    }catch (WrongSubscriberId wrongSubscriberId){
                        System.out.println("Wrong subscriber id!");
                    }
                    //

                    break;
                case "createCharacter":
                    //
                    String gender, birthDate;
                    ArrayList<String> partsList = getStringsBetweenDoubleQuotes(inputLine);
                    try {
                        name = partsList.get(0);
                        gender = partsList.get(1);
                        birthDate = partsList.get(2);

                        PersonalInformation personalInformation = new PersonalInformation(name, gender, birthDate);
                        id = newspaperOffice.getAllSubscribers().size();
                        newspaperOffice.getAllSubscribers().add(new Subscriber(id, personalInformation));
                        System.out.println(toStringPersonalInfo(id, name, birthDate, gender));
                    } catch (IndexOutOfBoundsException indexOutOfBoundsException) {
                        System.out.println("Wrong input format , consider entering names with double qoutations like \"sport\"");
                    }
                    break;
                case "getEditingProgress":
                    //
                    JournalModel jm = null;
                    try {
                        name = getStringsBetweenDoubleQuotes(inputLine).get(0);
                        jm = newspaperOffice.getJournalByName(name);
                        if (jm.getState() instanceof Editing)
                            System.out.println(((Editing) jm.getState()).progress());
                        else
                            System.out.println("Wrong state!");
                    } catch (IndexOutOfBoundsException indexOutOfBoundsException){
                        System.out.println("Wrong input format , consider entering names with double qoutations like \"sport\"");
                    }

                    break;
                case "startPrinting":
                    //
                    try {
                        name = getStringsBetweenDoubleQuotes(inputLine).get(0);
                        jm = newspaperOffice.getJournalByName(name);
                        if (jm.getState() instanceof Editing) {
                            jm.setState(new Printing());
                            System.out.println("State has changed to Printing");
                        } else
                            System.out.println("Wrong state!");
                        break;
                    } catch (IndexOutOfBoundsException indexOutOfBoundsException){
                        System.out.println("Wrong input format , consider entering names with double qoutations like \"sport\"");
                    }
                case "getPrintingProgress":
                    //
                    try {
                        name = getStringsBetweenDoubleQuotes(inputLine).get(0);
                        jm = newspaperOffice.getJournalByName(name);
                        if (jm.getState() instanceof Printing)
                            System.out.println(((Printing) jm.getState()).progress());
                        else
                            System.out.println("Wrong state!");
                        break;
                    } catch (IndexOutOfBoundsException indexOutOfBoundsException){
                        System.out.println("Wrong input format , consider entering names with double qoutations like \"sport\"");
                    }
                case "startPublishing":
                    //
                    try {
                        name = getStringsBetweenDoubleQuotes(inputLine).get(0);
                        jm = newspaperOffice.getJournalByName(name);
                        if (jm.getState() instanceof Printing) {
                            jm.setState(new Publishing(jm));
                            System.out.println("State has changed to Publishing");
                        } else
                            System.out.println("Wrong state!");
                        break;
                    }catch (IndexOutOfBoundsException indexOutOfBoundsException){
                        System.out.println("Wrong input format , consider entering names with double qoutations like \"sport\"");
                    }
                case "publish":
                    //
                    try {
                        name = getStringsBetweenDoubleQuotes(inputLine).get(0);
                        String status = getStringsBetweenDoubleQuotes(inputLine).get(1);
                        jm = newspaperOffice.getJournalByName(name);
                        if (jm.getState() instanceof Publishing) {
                            jm.getDeliveryStrategy().deliver((Publishing) jm.getState());
                            ((Publishing) jm.getState()).notifySubscribers(status);
                        } else
                            System.out.println("Wrong state!");
                        break;
                    } catch (IndexOutOfBoundsException indexOutOfBoundsException){
                        System.out.println("Wrong input format , consider entering names with double qoutations like \"sport\"");
                    } catch (NullPointerException np){
                        System.out.println("Could not find model");
                    }
                case "setPublishingMethod":
                    //
                    try {
                        partsList = getStringsBetweenDoubleQuotes(inputLine);
                        name = partsList.get(0);
                        String methodName = partsList.get(1);
                        DeliverStrategy dS = DeliverStrategy.getDeliveryStrategyByName(methodName);
                        newspaperOffice.getJournalByName(name).setDeliveryStrategy(dS);
                        System.out.println(name + "'s publishing method has changed to "+methodName);
                    } catch (IndexOutOfBoundsException indexOutOfBoundsException){
                        System.out.println("Wrong input format , consider entering names with double qoutations like \"sport\"");
                    } catch (NullPointerException np){
                        System.out.println("Could not find model");
                    }catch (DeliveryMethodNotFound methodNotFound){
                        System.out.println("Delivery method you entered not found");
                    }
                    break;
                case "exit":
                    System.out.println("BYE!");
                    System.exit(0);
                    break;
                default:
                    System.out.println("Invalid command");
            }
        }
    }


    public static String toStringPersonalInfo(int id, String name, String birthDate, String gender) {
        return "Character " + id + " has been created\n" +
                "name: " + name + "\ngender: " + gender + "\nbirth date: " + birthDate;
    }

    /* ----------------------------------------------------------------------------
        method getStringsBetweenDoubleQuotes
        gets String as input and finds words between double quotations and returns them in an array

     ------------------------------------------------------------------------------*/

    public static ArrayList<String> getStringsBetweenDoubleQuotes(String inputLine) {
        
        Pattern p = Pattern.compile("\"([^\"]*)\"");
        Matcher m = p.matcher(inputLine);
        ArrayList<String> result = new ArrayList<>();
        while (m.find()) {
            result.add(m.group(1));
        }
        return result;
    }

}
