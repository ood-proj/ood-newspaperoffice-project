package newspaperoffice;

import java.text.SimpleDateFormat;
import java.util.*;

/*  -----------------------------------------------------------------------------------
    class JournalModel
    implemented with multiton design pattern
    uses name property as key for creating new model instances and accordingly only allows one model instance per name

 -----------------------------------------------------------------------------------*/
public class JournalModel extends OutputString {


    private String name;
    private String date;
    private String head;
    private JournalState state;
    private ArrayList<SubscriberInfo> subscriberInfos = new ArrayList<>();
    private ArrayList<Subscriber> subscribers = new ArrayList<>();
    private DeliverStrategy deliveryStrategy;

    private static HashSet<String> allowedModelNames = new HashSet<>(Arrays.asList("political", "sport", "economical")); // to allow certain names as key
    private static HashMap<String, JournalModel> createdModels = new HashMap<>(); // to control instances


    private JournalModel(String name, String date, String head, JournalState startState, DeliverStrategy deliverStrategy) {
        this.name = name;
        this.date = date;
        this.head = head;
        this.state = startState;
        this.deliveryStrategy = deliverStrategy;

    }


    /*  -----------------------------------------------------------------------------------
        class JournalModelFriends
        to allow creating new instances of JournalModel to classes
     -----------------------------------------------------------------------------------*/
    public static class JournalModelFriends {
        /*  -----------------------------------------------------------------------------------
        method getJournalInstanceByName
        to get instance for NewspaperOffice accepts name as argument and initializes other properties with default hardcoded settings
        -----------------------------------------------------------------------------------*/
        public JournalModel getJournalInstanceByName(String name) throws JournalModelAlreadyExists, JournalModelNameNotAllowed {
            if (allowedModelNames.contains(name)) {
                if (createdModels.containsKey(name)) {
                    throw new JournalModelAlreadyExists();
                } else {
                    String nowDate = new SimpleDateFormat("dd-MM-yyyy").format(new Date());
                    JournalModel newInstance = new JournalModel(name, nowDate, "HeadMaster", new Editing(), new BikeDelivery()); // TODO : change headmaster name?!
                    createdModels.put(name, newInstance);
                    return newInstance;
                }

            } else {
                throw new JournalModelNameNotAllowed();
            }

        }

        /*  -----------------------------------------------------------------------------------
        method getJournalInstance
        to get instance for JournalModelSaver accepts all properties as argument
        -----------------------------------------------------------------------------------*/
        public JournalModel getJournalInstance(String name, String date, String head, JournalState state, DeliverStrategy deliveryStrategy) throws JournalModelAlreadyExists, JournalModelNameNotAllowed {

            if (allowedModelNames.contains(name)) {
                if (createdModels.containsKey(name)) {
                    throw new JournalModelAlreadyExists();
                } else {
                    JournalModel newInstance = new JournalModel(name, date, head, state, deliveryStrategy);
                    createdModels.put(name, newInstance);
                    return newInstance;
                }

            } else {
                throw new JournalModelNameNotAllowed();
            }

        }

        private JournalModelFriends() {
        } // no public constructor
    }

    public static void giveKeyTo(JournalModelSaver jmSaver) {
        jmSaver.receiveKey(new JournalModelFriends());
    }

    public static void giveKeyTo(NewspaperOffice office) {
        office.receiveKey(new JournalModelFriends());
    }

    /*  -----------------------------------------------------------------------------------
            method registerSubscriber
            encodes subscriber's personal data using SubscriberInfoData adapter
            adds subscriber to this model's list
            returns encoded Subscriber info
            -----------------------------------------------------------------------------------*/
    public SubscriberInfo registerSubscriber(Subscriber s) {
        String subscriberData = s.getId() + " " + SubscriberInfoData.getEncodedData(s.getPersonalInformation());
        SubscriberInfo newInfoEntry = new SubscriberInfo(subscriberData);
        subscriberInfos.add(newInfoEntry);
        this.subscribers.add(s);
        return newInfoEntry;
    }

    public boolean subscribed(int id) {
        int current_id;
        for (SubscriberInfo i : this.subscriberInfos) {
            current_id = Integer.parseInt(i.getData().split(" ")[0]);
            if (current_id == id)
                return true;
        }
        return false;
    }

    public DeliverStrategy getDeliveryStrategy() {
        return deliveryStrategy;
    }

    public void setDeliveryStrategy(DeliverStrategy deliveryStrategy) {
        this.deliveryStrategy = deliveryStrategy;
    }


    public String getName() {
        return name;
    }


    public String getDate() {
        return date;
    }


    public String getHead() {
        return head;
    }

    public JournalState getState() {
        return state;
    }

    public void setState(JournalState state) {
        this.state = state;
    }

    public ArrayList<SubscriberInfo> getSubscriberInfos() {
        return subscriberInfos;
    }

    public void addNewSubscriberInfo(String subscriberData) {
        SubscriberInfo retrievedInfoEntry = new SubscriberInfo(subscriberData);
        subscriberInfos.add(retrievedInfoEntry);
    }

    public void addNewSubscriberReference(Subscriber s) {
        this.subscribers.add(s);
    }

    @Override
    public String modelToString() {
        return "Name: " + name + "\n" +
                "Date Established: " + date + "\n" +
                "Hashcode: " + this.hashCode() + "\n" +
                "Head: " + head + "\n"
                ;
    }

    @Override
    public String toString() {
        return "JournalModel{" +
                "name='" + name + '\'' +
                ", date='" + date + '\'' +
                ", head='" + head + '\'' +
                ", state=" + state +
                ", subscriberInfos=" + subscriberInfos +
                '}';
    }

    public ArrayList<Subscriber> getSubscribers() {
        return subscribers;
    }

}

class JournalModelAlreadyExists extends RuntimeException {


}

class JournalModelNameNotAllowed extends RuntimeException {


}