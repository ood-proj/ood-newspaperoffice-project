package newspaperoffice;

import java.util.Random;

public class Printing extends JournalState {
    private int percent;
    public int progress(){
        Random rand = new Random();
        percent = rand.nextInt( 101-percent)+percent;
        return percent;
    }
}
