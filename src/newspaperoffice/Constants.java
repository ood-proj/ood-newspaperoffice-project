package newspaperoffice;

public class Constants {

    public static class DecoratorName{
        public static final String Q_DECORATOR = "ADDQ";
        public static final String STAR_DECORATOR = "ADDSTAR";

    }
    public static class DeliveryMethodStrings{
        public static final String BIKE_DELIVERY = "courier";
        public static final String ONLINE_DOWNLOAD = "online";

    }
}
