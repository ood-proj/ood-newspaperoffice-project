package newspaperoffice;

import java.io.IOException;
import java.util.ArrayList;

public class SubscriberSaver extends ObjectSaver {
    private static ObjectSaver uniqueInstance;
    private SubscriberSaver(String fileName) {
        this.fileName = fileName;
    }

    public static ObjectSaver getUniqueInstance(String fileName) {
        if (uniqueInstance == null) {
            uniqueInstance = new SubscriberSaver(fileName);
        }
        return uniqueInstance;
    }

    public void saveList(ArrayList<Subscriber> subscribers) {
        ArrayList<String[]> dataLines = new ArrayList<>();
        for (Subscriber s : subscribers) {
            ArrayList<String> personalInfoString = new ArrayList<>();
            personalInfoString.add(s.getPersonalInformation().getName());
            personalInfoString.add(s.getPersonalInformation().getGender());
            personalInfoString.add(s.getPersonalInformation().getBirthDate());

            dataLines.add(personalInfoString.toArray(new String[0]));
        }

        try {
            saveDataArrayToFile(dataLines);
        } catch (IOException e) {
            e.printStackTrace();
        }


    }

    public ArrayList<Subscriber> retrieve() {
        ArrayList<Subscriber> result = new ArrayList<>();
        ArrayList<String[]> csvContent = this.readFromCSV();

        for (String[] objContent : csvContent) {

            PersonalInformation personalInformation = new PersonalInformation(objContent[0], objContent[1], objContent[2]);
            result.add(new Subscriber(result.size(), personalInformation));
        }
        return result;
    }

}
