package newspaperoffice;

import java.io.*;
import java.util.ArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public abstract class ObjectSaver {
    protected String fileName;
    protected ObjectSaver(){}

    public String convertToCSV(String[] data) {
        return Stream.of(data)
                .map(this::escapeSpecialCharacters)
                .collect(Collectors.joining(","));
    }
    public void saveDataArrayToFile(ArrayList<String[]> dataLines) throws IOException {
        File csvOutputFile = new File(fileName);
        try (PrintWriter pw = new PrintWriter(csvOutputFile)) {
            dataLines.stream()
                    .map(this::convertToCSV)
                    .forEach(pw::println);
        }

        assert csvOutputFile.exists();
    }
    public ArrayList<String[]> readFromCSV(){
        ArrayList<String[]> result = new ArrayList<String[]>();
        BufferedReader csvReader = null;
        String row = "";
        try {
            csvReader = new BufferedReader(new FileReader(this.fileName));
            while ((row = csvReader.readLine()) != null) {
                result.add(row.split(","));
            }
            csvReader.close();
        } catch (FileNotFoundException e) {
            File file = new File(this.fileName);
            try {
                if(file.createNewFile()){
                    System.out.println("file.txt File Created in Project root directory");
                }
            } catch (IOException ioException) {
                System.out.println("LOG : Could not create new file");
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return result;
    }
    public String escapeSpecialCharacters(String data) {
        String escapedData = data.replaceAll("\\R", " ");
//        if (data.contains(",") || data.contains("\"") || data.contains("'")) {
//            data = data.replace("\"", "\"\"");
//            escapedData = "\"" + data + "\"";
//        }
        return escapedData;
    }
}
