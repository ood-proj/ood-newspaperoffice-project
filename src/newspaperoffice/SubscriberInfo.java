package newspaperoffice;

public class SubscriberInfo {
    private String data;

    public SubscriberInfo(String data) {
        this.data = data;
    }

    public String getData() {
        return data;
    }

    @Override
    public String toString() {
        return getData();
    }
}
